<?php require("koneksi.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <title>Pendaftaran Training | IT man's Maintenance WORKSHOP</title>
</head>

<body>
    <header>
        <h3>Para Peserta Training</h3>
        <h4>IT man's Maintenance WORKSHOP</h1>
    </header>

    <nav>
    	<a href="formulir-pendaftaran.php">[+] Tambah Peserta</a>
    </nav>

    

    <br>


    <table border="1">
    	<thead>
    		<tr>
    			<th>No</th>
    			<th>Nama</th>
    			<th>Email</th>
    			<th>No Telepon</th>
    			<th>Alamat</th>
    			<th>Jenis Kelamin</th>
    			<th>Tempat Lahir</th>
    			<th>Tanggal Lahir</th>
    			<th>Jenis Instansi</th>
    			<th>Nama Instansi</th>
    			<th>Tanggal Training</th>
    			<th>Pembayaran</th>
    		</tr>
    	</thead>
    	<tbody>


    		
    		<?php 
            
            $sql = "SELECT * FROM parapendaftar ";
    		$query = mysqli_query($konek, $sql);
            $queryres = mysqli_num_rows($query);

            if ($queryres > 0) {
                while ($peserta = mysqli_fetch_assoc($query)) {

            echo "<tr>";

            echo "<td>".$peserta['id']."</td>";
            echo "<td>".$peserta['nama']."</td>";
            echo "<td>".$peserta['email']."</td>";
            echo "<td>".$peserta['no_telp']."</td>";
            echo "<td>".$peserta['alamat']."</td>";
            echo "<td>".$peserta['jenis_kelamin']."</td>";
            echo "<td>".$peserta['tempat_lahir']."</td>";
            echo "<td>".$peserta['tanggal_lahir']."</td>";
            echo "<td>".$peserta['jenis_instansi']."</td>";
            echo "<td>".$peserta['nama_instansi']."</td>";
            echo "<td>".$peserta['tanggal_training']."</td>";
            echo "<td>".$peserta['pembayaran']."</td>";


            echo "<td>";
            echo "<a href='script-delete.php?id=". $peserta['id']."'>Hapus</a> ";
            echo "</td>";
                
            echo "</tr>";
                }
            }

    		//while ($peserta = mysqli_fetch_array($query)) {
    			
    	//	}

    		 ?>

    	</tbody>

    </table>
    <br>

    <form action="" method="POST">
        
        <input type="text" name="search" placeholder="Search..." autocomplete="off">
        <button type="submit" name="cari">Cari Nama Peserta</button>

    </form>
    <?php 
        if (isset($_POST['cari']))  {
            $search = mysqli_real_escape_string($konek, $_POST['search']);
            $sql = "SELECT * FROM parapendaftar WHERE nama LIKE '%$search%'";
            $query = mysqli_query($konek, $sql);
            $queryres = mysqli_num_rows($query);

    ?>
    <table border="1">
    <?php  
            if ($queryres > 0) {
                while ($peserta = mysqli_fetch_assoc($query)) {
   
                echo "<thead>";
                echo "<tr>"; 
                echo "<th>No</th>";   
                echo "<th>Nama</th>";
                echo "<th>Email</th>";
                echo "<th>No Telepon</th>";
                echo "<th>Alamat</th>";
                echo "<th>Jenis Kelamin</th>";
                echo "<th>Tempat Lahir</th>";
                echo "<th>Tanggal Lahir</th>";
                echo "<th>Jenis Instansi</th>";
                echo "<th>Nama Instansi</th>";
                echo "<th>Tanggal Training</th>";
                echo "<th>Pembayaran</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";

                echo "<tr>";

                echo "<td>".$peserta['id']."</td>";
                echo "<td>".$peserta['nama']."</td>";
                echo "<td>".$peserta['email']."</td>";
                echo "<td>".$peserta['no_telp']."</td>";
                echo "<td>".$peserta['alamat']."</td>";
                echo "<td>".$peserta['jenis_kelamin']."</td>";
                echo "<td>".$peserta['tempat_lahir']."</td>";
                echo "<td>".$peserta['tanggal_lahir']."</td>";
                echo "<td>".$peserta['jenis_instansi']."</td>";
                echo "<td>".$peserta['nama_instansi']."</td>";
                echo "<td>".$peserta['tanggal_training']."</td>";
                echo "<td>".$peserta['pembayaran']."</td>";


                echo "<td>";
                echo "<a href='script-delete.php?id=". $peserta['id']."'>Hapus</a> ";
                echo "</td>";
                
                echo "</tr>";

                echo "</tbody>";    

                echo "</table>";
                }
            } else {
                echo "Tidak ada Hasil";
            }
        }


    ?>

    <p>Total: <?php echo mysqli_num_rows($query) ?></p>
    <p><a href="index.php">Kembali ke halama utama</a></p>

    </body>
</html>